import glob
import os
import re
import shutil

# IE10+:
# 
# A stylesheet may contain up to 65,534 selectors
# A document may load up to 4095 stylesheets
# @import nesting is limited to 4095 levels (due to the 4095 stylesheet limit)
# The IE10 Dev Guide says the limits for IE10 have been removed entirely, but in the comments of his blog post on the IE limits, Eric Law says these limits for IE10 are accurate. Again, I don't have a test case for this.

###### Bugs: ########
#1 script will create more class tags, which will be ignored by browser in case tag already contains class attribute without quotes like class=trans_table
#1 such classes can be fixed manually after searching using regex: class\s?=\s?\w+?\s+[^>]*?class\s?=
#2 in theory can change the priority of the styles because inline styles have higher priority than class

srcDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/inline-style-attributes/raw"
targetDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/inline-style-attributes/processed"
jspFilePaths = glob.glob("%s/*.jsp" % srcDir)
jspFilePaths.extend(glob.glob("%s/*.htm" % srcDir))
jspFilePaths.extend(glob.glob("%s/*.html" % srcDir))

js_inline_style_attributes = r"(?:^|\s)(style)\s*?=\s*([\"'])\s*((?:[\s\S])*?)\s*\2"
css_class_attributes_regex = r"(?:^|\s)(class)\s*?=\s*([\"'])\s*((?:[\s\S])*?)\s*\2"
all_jsp_tags_regex = r"<(\s*(\w+)((?:(?:(?:.|\s)<%|[^<])*?)(?:[^%]|\d%\s?)))>"
head_closing_tag_regex = r"</head\s*>"


def clear_dir(directory):
    shutil.rmtree(directory, ignore_errors=True)
    os.makedirs(directory)
    os.makedirs(directory + "/css")
    os.makedirs(directory + "/markup")


def write_to_file(target_dir, file_name, content):
    new_file = open("%s/%s" % (target_dir, file_name), "w")
    new_file.write(content)
    new_file.close()


def remove_inline_styles(jspFileText):
    return re.sub(all_jsp_tags_regex, removeInlineHandlersFromTag, jspFileText, flags=re.M | re.I)


def removeInlineHandlersFromAttributes(attributes):
    return re.sub(js_inline_style_attributes, "", attributes, flags=re.M | re.I)


def removeInlineHandlersFromTag(matchObj):
    tag = matchObj.group(0)
    if not bool(re.search(js_inline_style_attributes, tag, flags=re.I | re.M)): return tag
    tag_name = matchObj.group(2)
    attributes = matchObj.group(3)
    closing_slash = matchObj.group(4)
    if closing_slash is None: closing_slash = ''
    cleaned_attributes = removeInlineHandlersFromAttributes(attributes)
    return "<%s %s%s>" % (tag_name, cleaned_attributes, closing_slash)


clear_dir(targetDir)


def get_css_class_name(file_name, file_extension, tag_count):
    class_prefix = file_name
    if file_extension.lower() != '.jsp': class_prefix = file_name + '-' + file_extension[1:]

    return "%s-%s" % (class_prefix, tag_count)


def get_css_classes(tag):
    whole_tag = tag[0]
    tag_attributes = tag[2]
    css_class_attributes = re.findall(css_class_attributes_regex, tag_attributes, flags=re.M | re.I)
    if (len(css_class_attributes) == 0): return ""
    if (len(css_class_attributes) > 1): raise Exception("more than one class attribute for tag: %s" % whole_tag)
    return css_class_attributes[0][2]


def add_css_class(jsp_text, tag, css_class_name):
    existing_tag = tag[0]
    tag_name = tag[1]
    tag_attributes = tag[2]
    closing_slash = tag[3]
    if closing_slash is None: closing_slash = ''
    existing_css_classes = get_css_classes(tag)
    classes = (existing_css_classes + " %s" % css_class_name).strip()
    new_class_attribute = " class='%s'" % classes
    if (len(existing_css_classes) > 0):
        new_tag = re.sub(css_class_attributes_regex, new_class_attribute, existing_tag, flags=re.M | re.I)
    else:
        new_tag = "<%s %s%s%s>" % (tag_name, tag_attributes, new_class_attribute, closing_slash)

    return re.sub(re.escape(existing_tag), new_tag, jsp_text, flags=re.M | re.I)


def append_css_class(css_file_content, css_class_name, inline_style_entries):
    css_class_body = ";\n    ".join(inline_style_entries) + ";"

    return css_file_content + "\n.%s {\n" \
                              "    %s\n" \
                              "}\n" % (css_class_name, css_class_body)


def get_class_from_cache(css_class_cache, inline_style_entries):
    for class_name, cached_style_entries in css_class_cache.items():
        if len(cached_style_entries) != len(inline_style_entries): continue
        if cached_style_entries.issubset(inline_style_entries): return class_name
    return ""


def add_style_link(jsp_text, style_name):
    style_link = "<link rel='stylesheet' type='text/css' href='./css/raw/inline/%s' />\n" % style_name
    if len(re.findall(head_closing_tag_regex, jsp_text, flags=re.M | re.I))>0:
        head_closing_tag_with_link = "\n    %s</head>" % style_link
        return re.sub(head_closing_tag_regex, head_closing_tag_with_link, jsp_text, 1, flags=re.M | re.I)
    else:
        return style_link + jsp_text


for jspPath in jspFilePaths:
    with open(jspPath, 'r') as jspFile:
        print("processing %s" % jspFile)
        file_name, file_extension = os.path.splitext(os.path.basename(jspPath))
        jspFileText = jspFile.read()
        changedJspText = jspFileText
        allTags = re.findall(all_jsp_tags_regex, changedJspText, flags=re.M | re.I)
        if len(allTags) == 0: continue
        tag_count = 0
        cssFileContent = ""
        css_class_cache = {}
        for tag in allTags:
            tag_count += 1
            full_match = tag[0]
            tag_name = tag[1]
            tagAttributes = tag[2]
            inlineStyles = re.findall(js_inline_style_attributes, tagAttributes, flags=re.M | re.I)
            if len(inlineStyles) > 1: raise Exception("%s multiple style attributes in tag %s" % (file_name + "." + file_extension, full_match))
            if len(inlineStyles) == 0: continue
            inlineStyle = inlineStyles[0]
            inline_style_body = inlineStyle[2]
            if len(inline_style_body) == 0: continue
            inline_style_entries = inline_style_body.split(";")
            inline_style_entries = set(map(lambda str: str.strip(), inline_style_entries))
            if "" in inline_style_entries: inline_style_entries.remove("")

            css_class_name = get_class_from_cache(css_class_cache, inline_style_entries)
            if css_class_name == "":
                css_class_name = get_css_class_name(file_name, file_extension, tag_count)
                css_class_cache[css_class_name] = inline_style_entries
                cssFileContent = append_css_class(cssFileContent, css_class_name, inline_style_entries)
            changedJspText = add_css_class(changedJspText, tag, css_class_name)

        if len(cssFileContent) != 0:
            style_prefix = file_name
            if file_extension.lower() != '.jsp': style_prefix = file_name + '_' + file_extension[1:]
            style_name = "%s.css" % style_prefix
            write_to_file(targetDir, "css/%s" % (style_name), cssFileContent)
            changedJspText = remove_inline_styles(changedJspText)
            changedJspText = add_style_link(changedJspText, style_name)
            write_to_file(targetDir, "markup/%s%s" % (file_name, file_extension), changedJspText)
