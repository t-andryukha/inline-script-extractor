import glob
import os
import re
import shutil

# Known issues:
# in case of jsp and html file name collision, only single version of js file will be preserved

srcDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/resources/raw"
targetDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/resources/processed"
js_script_regex = r"<script\s*?(?![^>]*?src)[^>]*?>((.|\n)+?)<\/script>"


def clear_dir(directory):
    shutil.rmtree(directory, ignore_errors=True)
    os.makedirs(directory)
    os.makedirs(directory + "/js")
    os.makedirs(directory + "/markup")


def write_to_file(target_dir, file_name, content):
    new_file = open("%s/%s" % (target_dir, file_name), "w")
    new_file.write(content)
    new_file.close()


clear_dir(targetDir)

jspFilePaths = glob.glob("%s/*.jsp" % srcDir)
jspFilePaths.extend(glob.glob("%s/*.htm" % srcDir))
jspFilePaths.extend(glob.glob("%s/*.html" % srcDir))


def get_jsp_text_without_inline_script(changedJspText, numOfFile, scriptPrefix):
    return re.sub(js_script_regex, '<script src="./javascript/raw/%s%s.js"></script>' % (scriptPrefix, numOfFile), changedJspText, 1, flags=re.M | re.I) 


for jspPath in jspFilePaths:
    with open(jspPath, 'r') as jspFile:
        file_name, file_extension = os.path.splitext(os.path.basename(jspPath))
        jspFileText = jspFile.read()
        jsInlineScripts = re.findall(js_script_regex, jspFileText, flags=re.M | re.I)
        if len(jsInlineScripts) == 0: continue
        changedJspText = jspFileText
        jsFileCounter = 0
        for jsInlineScript in jsInlineScripts:
            jsFileCounter += 1
            numOfFile = ''
            if jsFileCounter > 1: numOfFile = jsFileCounter
            jsFileContent = jsInlineScript[0]  # first regex group from each script
            scriptPrefix = file_name
            if file_extension.lower() != '.jsp': scriptPrefix = file_name + '_' + file_extension[1:]
            changedJspText = get_jsp_text_without_inline_script(changedJspText, numOfFile, scriptPrefix)
            write_to_file(targetDir, "js/%s%s.js" % (scriptPrefix, numOfFile), jsFileContent)
            
        write_to_file(targetDir, "markup/%s%s" % (file_name, file_extension), changedJspText)
