import glob
import os
import re
import shutil

# BUG: SCRIPT can have false-positives in JSP because of scriptlets, for example: <% if(a < b) ... String oneVariableThatLooksLikeHandler="it looks like handler code";...
# Possible BUG: handlers for embedded jsps with conditional body (html body inside java's if) will still be set regardless of condition, because it's not taken into account anyhow 
# Html and JSP file collistions result in js file with same name

srcDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/inline-handlers/raw"
jsSrcDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/inline-handlers/raw-js"
targetDir = "/home/NIX/t-andryukha/workspace/inline-scripts-extractor/inline-handlers/processed"
jspFilePaths = glob.glob("%s/*.jsp" % srcDir)
jspFilePaths.extend(glob.glob("%s/*.htm" % srcDir))
jspFilePaths.extend(glob.glob("%s/*.html" % srcDir))

js_inline_handlers_regex = r"(?:^|\s)(on\S+?)\s*?=\s*([\"']?)\s*((?:[\s\S])*?)\s*?\2(?:[^%]>|\s|$)"
all_jsp_tags_regex = r"<(\s*(\w+)((?:(?:(?:.|\s)<%|[^<])*?)(?:[^%]|\d%\s?)))>"


def clear_dir(directory):
    shutil.rmtree(directory, ignore_errors=True)
    os.makedirs(directory)
    os.makedirs(directory + "/js")
    os.makedirs(directory + "/markup")


def write_to_file(target_dir, file_name, content):
    new_file = open("%s/%s" % (target_dir, file_name), "w")
    new_file.write(content)
    new_file.close()


def removeInlineHandlers(jspFileText):
    return re.sub(all_jsp_tags_regex, removeInlineHandlersFromTag, jspFileText, flags=re.M | re.I)


def removeInlineHandlersFromAttributes(attributes):
    return re.sub(js_inline_handlers_regex, "", attributes, flags=re.M | re.I)


def removeInlineHandlersFromTag(matchObj):
    tag = matchObj.group(0)
    if not bool(re.search(js_inline_handlers_regex, tag, flags=re.I | re.M)): return tag
    tag_name = matchObj.group(2)
    attributes = matchObj.group(3)
    cleaned_attributes = removeInlineHandlersFromAttributes(attributes)
    return "<%s %s>" % (tag_name, cleaned_attributes)


def createUid(file_name, tag_name, tag_count):
    version_suffix = "v2"
    return '%s_%s_%s_%s' % (file_name, tag_name.lower(), tag_count, version_suffix)


def addUid(jsp_text, tag, uid):
    existing_tag = tag[0]
    tag_name = tag[1]
    tag_attributes = tag[2]
    # todo combine with removing inline handlers
    new_tag = "%s data-uid='%s' %s" % (tag_name, uid, tag_attributes)
    return re.sub(re.escape(existing_tag), new_tag, jsp_text, flags=re.M | re.I)


clear_dir(targetDir)
for jspPath in jspFilePaths:
    with open(jspPath, 'r') as jspFile:
        print("processing %s" % jspFile)
        file_name, file_extension = os.path.splitext(os.path.basename(jspPath))
        jspFileText = jspFile.read()
        changedJspText = jspFileText
        allTags = re.findall(all_jsp_tags_regex, changedJspText, flags=re.M | re.I)
        if len(allTags) == 0: continue
        tag_count = 0
        jsFileContent = ""
        jspChanged = False
        for tag in allTags:
            tag_count += 1
            full_match = tag[0]
            tag_name = tag[1]
            tagAttributes = tag[2]
            inlineHandlers = re.findall(js_inline_handlers_regex, tagAttributes, flags=re.M | re.I)
            if len(inlineHandlers) == 0: continue
            jspChanged = True
            for inlineHandler in inlineHandlers:
                handlerName = inlineHandler[0].strip().lower()
                handlerCode = inlineHandler[2]
                if handlerName == 'onload':
                    jsFileContent += "function %s_%s (){\n" \
                                     "    %s" \
                                     "\n}" \
                                     "\n%s_%s();" \
                                     "\n" % (handlerName, tag_name, handlerCode, handlerName, tag_name)
                elif tag_name.lower() == 'body':
                    # will assign handler to the outer body even in case this body is under scriptlet if and will never be rendered. Need to be careful with this case
                    # may result in a bug if handlers were inside java i                       f for the body.
                    jsFileContent += "document.body['%s'] = function(){\n" \
                                     "    %s" \
                                     "\n}" \
                                     "\n" % (handlerName, handlerCode)
                else:
                    elementUid = createUid(file_name, tag_name, tag_count)
                    changedJspText = addUid(changedJspText, tag, elementUid)
                    if elementUid not in jsFileContent:
                        jsFileContent += "var %s_collection = document.querySelectorAll(\"[data-uid='%s']\");\n" % (elementUid, elementUid)
                    jsFileContent += "for (var i = 0; i < %s_collection.length; i++) {\n" \
                                     "    var %s = %s_collection[i];\n" \
                                     "    %s['%s'] = function(event){\n" \
                                     "        %s" \
                                     "\n    }" \
                                     "\n}" \
                                     "\n" % (elementUid, elementUid, elementUid, elementUid, handlerName, handlerCode)
        if len(jsFileContent) != 0:
            scriptPrefix = file_name
            if file_extension.lower() != '.jsp': scriptPrefix = file_name + '_' + file_extension[1:]
            scriptName = "%sInlineScripts.js" % scriptPrefix
            scriptLink = '\n\n<script defer src="./javascript/raw/handlers/%s"></script>' % scriptName
            if not changedJspText.endswith(scriptLink): changedJspText += scriptLink
            existing_file_path = jsSrcDir + "/" + scriptName
            if os.path.isfile(existing_file_path):
                existing_js_file = open(existing_file_path, 'r')
                existing_js_content = existing_js_file.read()
                jsFileContent = existing_js_content + jsFileContent
                existing_js_file.close()
            write_to_file(targetDir + "/js", scriptName, jsFileContent)
        if jspChanged:
            changedJspText = removeInlineHandlers(changedJspText)
            write_to_file(targetDir + "/markup", "%s%s" % (file_name, file_extension), changedJspText)
